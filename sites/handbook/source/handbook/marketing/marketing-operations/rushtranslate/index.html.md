---
layout: handbook-page-toc
title: "RushTranslate"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

RushTranslate is a SaaS translation services primarily used, owned, and operated by the content marketing team. 

## Ordering

When placing an order for translation, please have an accompanying issue link attached to the order in RushTranslate and copy/paste the order number from RushTranslate to your respective issue. 

### Workflow 

1. Create an issue in the [growth-marketing project](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues) using the `translation-tracking` template. 
1. Tag your manager for budget approval. 
1. Once you have manager approval, submit your document for translation and add the order number to the issue. 
1. Attach the translated copy to the issue. 
1. Close the issue. 