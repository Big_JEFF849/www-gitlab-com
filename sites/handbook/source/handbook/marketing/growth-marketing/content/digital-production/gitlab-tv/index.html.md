---
layout: handbook-page-toc
title: "GitLab TV"
description: "Handbook page for GitLab TV"
twitter_image: '/images/tweets/handbook-marketing.png'
---

This is a public-facing document that outlines the GitLab.tv MVC strategy and contribution guidelines.

GitLab.tv is GitLab’s video-first content platform focused on generating valuable and viral content by enabling and encouraging contribution and distribution of GitLab video content. We currently use the GitLab Branded YouTube as our video-first platform.

## GitLab.tv Goals

1.** Grow brand awareness and business.**
GitLab.tv is focused on growing our business and brand in measurable ways. Content is strategically focused on attracting, converting, nurturing, or closing leads. We want to provide valuable content and then invite viewers to continue their DevOps adoption and GitLab journey through conversion paths and CTAs.
1. **Encourage contribution of customer-centric and shareable content.**
GitLab.TV is a group effort. We encourage contribution by providing streamlined paths and guidelines for contribution. We enable contributors to create customer-centric content that is easy to find and consume and is aligned to audience needs.
1. **Increase video content distribution.**
GitLab.tv video content is distributed across owned, earned and paid content channels. All GitLab.tv video content contributions that are approved are first posted to their respective Gitlab.tv Video Channels on YouTube. GitLab.tv video content can also be cross-promoted via GitLab websites, blog, social, and third party platforms as determined by GitLab.tv Video Channel Distribution plans. Top content may be promoted more than standard content. The goal is to make GitLab accessible to audiences, especially top content.

### Success metrics

1. Grow YouTube subscribers
1. Increase traffic from YouTube to about.gitlab.com
1. Average X views per video
1. Number (#) of website pages with video content

## Audience

Like all online audiences, the GitLab.tv audience is short on patience and long on content options, so they do not have patience for brands or content platforms that do not quickly meet or exceed their needs and expectations. Once they feel a brand or platform is aligned to their needs, their loyalty and patience increases. The GitLab.tv audience likes to share content they feel is valuable to others on their team.

The primary GitLab.tv audience are developers and DevOps practitioners. They are individual contributors and managers who are deep in the weeds of day to day execution of development and operations tasks. They are most interested in content that answers a specific question or goes into specifics on a single topic. They look for content that is straightforward, concise, and has clear learning objectives. This audience is willing to watch a 40+ minute video if it offers clear takeaways and/or new information that they can directly apply to their day to day work.

## Creating content for GitLab.tv
GitLab.tv strives to create valuable and viral content. In order to do that we need to make sure every video is customer-centric, from the script to the meta tags we add in YouTube to ensure it’s discoverable. Here are some goals to keep in mind when creating GitLab.tv content:
### Content guidelines
1. **Audience-centric:** Before creating your video, ask yourself how does this content help our audience achieve their goals? Are there clear takeaways? What is the next step someone should take after watching this video? Make sure that CTA is clear. 
1. **Optimize for search:** The majority of our video content is found organically. Make sure you are optimizing your videos for search by using descriptive titles and descriptions. For example, if you are publishing a video how to use GitLab Issue Boards for project management, use the title “How to use GitLab Issue Boards for project management” over “Senior Developer David Smith explains Issue Boards.
1. **Engaging, clear, and concise:** GitLab.tv video content is clear and concise but also enjoyable to consume and share. We should avoid internal jargon and follow other communication best practices laid out in our handbook. The videos should be easy to play, hear, and so on. 
1. **Micro-content:** GitLab.tv values micro-content over long-form content. Contributors can develop a series of micro-content pieces that allow each video to be played as stand-alone and or for the viewer to review the content series sequentially.
1. **Accessible:** GitLab.tv content should be easy for relevant audiences to find on GitLab.tv as well as in their preferred content channels.

For more specific information on video content types, refer to the [video playbook handbook](/handbook/communication/video-playbook/).

### Conversion paths & CTAs

All GitLab.tv video content should include a CTA. The goal is to provide frictionless conversion paths that are relevant to the viewer and easy to follow. Here’s how you can integrate CTAs in video content:

1. **End screen CTA:** Add a closing slide to your video with a call to action and link.
1. **Spoken CTA:** Call out the action you want the viewer to take in your video. For example, “check our eBook on XYZ topic to dive deeper” or “if you liked this video, make sure to subscribe for more.”
1. **On-screen graphics CTA:** Add graphics, like lower thirds or banners, to your video encouraging viewers to check out additional links.
1. **Text-based CTA:** Add text-based links to your video description.

## Distribution and promotion of GitLab.tv content

1. GitLab website
1. GitLab blog
1. Path Factory

## GitLab.tv Channels

A GitLab.tv Video Channel is a content track focused on a specific audience and specific business goals. Each channel has a lead who is responsible for:

- Building their channel team 
- Curating their channels video content 
- Distributing their content and promoting top content
- Reporting on channel and content success metrics

GitLab.tv channels can take many forms as long as they satisfy the GitLab.tv overall strategy, goals, and deliver ROI. Channel leads have the flexibility to define and experiment with channels of all types. The GitLab.tv team doesn’t know which channels will be successful, and none of us are as smart as all of us, so we’ve intentionally given GitLab.tv video channel leads flexibility to create and try channels of all kinds. Channels and their content are measured to gauge success, inform continual improvement and justify further investment. If a channel is not providing ROI, the GitLab.tv team may ask that it be retired. 

### Contribute to an existing channel

To contribute to an existing channel, visit the channel playbook for more information on contribution guidelines. 

1. All-Remote(link)
1. Product(link)
1. Events(link)
1. Community(link)

### Create a new channel

GitLab.tv Video Channel Playbooks define the channels audience, measurable goals, content types, content distribution plan and contribution paths. To create a GitLab.tv Video Channel Playbook, use this template <link> and reach out to the GitLab.tv team. 
