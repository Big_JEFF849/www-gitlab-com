function buildWebIdeHref() {
  const instance = document.querySelector('meta[property="og:instance"]').content
  const path = document.querySelector('meta[property="og:path"]').content
  const relativePath = document.querySelector('meta[property="og:relative_path"]').content

  return `${instance}-/ide/project/${path}edit/master/-/source/${relativePath}`
}

export default buildWebIdeHref