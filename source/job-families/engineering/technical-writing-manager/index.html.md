---
layout: job_family_page
title: "Technical Writing Management"
---

Technical Writing Managers in the UX department at GitLab see the team as their product. While they are credible as Technical Writers and know the details of what Technical Writers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of technical writing commitments and are always looking to improve productivity and process. They must also coordinate across departments to accomplish collaborative goals.

## Technical Writing Manager

The Technical Writing Manager reports to the Senior Manager, Technical Writing and a team of Technical Writers report to the Technical Writing Manager.

### Responsibilities

* Hire and manage a world-class team of Technical Writers.
* Promote a results-driven approach and work-life balance on the Technical Writing team by helping direct reports to balance assignments within the team when workloads are uneven, manage conflicting priorities, and prevent overresourcing.
* Deliver input on promotions, function changes, demotions, and offboarding in consultation with the other Technical Writing Managers, the UX Director, and the Senior Manager, Technical Writing.
* Make sure the team is focused on the right things by helping Technical Writers effectively balance high-priority feature release work with other content, experience, and process improvements.
* Make sure that every Technical Writer on their team has clear priorities from their PMs/PM Directors for each milestone.

#### Documentation

* Manage documentation improvement efforts. Coordinate regular content audits for documentation in your area of responsibility.
* Improve content usability by identifying opportunities to connect content that flows across multiple sections into end-to-end documentation that maps to GitLab [JTBDs](https://about.gitlab.com/handbook/engineering/ux/ux-resources/#jobs-to-be-done).
* Make sure by the time of release all documentation, guides, and related content were shipped.
* Help Technical Writers refine their writing skills by spot-checking content and providing editorial feedback.

#### Process Improvement

* Encourage efficient collaboration by defining processes that align with the rest of the product team, are consistent and efficient, and use GitLab capabilities to power process flows and metrics.
* Create practices that encourage keeping documentation up to date and easily discoverable.
* Promote and optimize the experience for documentation contributions from the GitLab team and the wider GitLab community.
* Create a self-managing framework for the Community Writers program.

### Requirements

* Experience in setting up online documentation is a hard requirement for this role
* Previous experience using Git is a hard requirement for this role
* Proven experience in managing, mentoring, and training teams of Technical Writers
* Experience working on production-level documentation
* Self-motivated and strong organizational skills
* Strong written and spoken communication skills
* Familiarity with Static Site Generators, HTML, CSS
* Experience with workflows and tooling used by engineering, operations, product teams
* Familiarity with Continuous Integration, Containers, Kubernetes, and project management software a plus
* Experience engaging with a wide range of stakeholders across a company, its customers, and/or open-source communities
* You share our [values](/handbook/values/) and work in accordance with those values
* [Adhere to our view of Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

### Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

### Hiring Process 

* Screening call with a recruiter.
* Interview with Technical Writer. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with cross-functional partners, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your approach to writing work, experience with docs-as-code workflows, and your technical abilities, too.
* Interview with Technical Writing Manager. In this interview, we want you to share a case study presentation that provides insight to a project you led. We'll look to understand the size and structure of your team, the goals of the project, the tool chain you used, how you managed the team and workload, and how you collaborated with cross-functional partners. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with cross-functional partners, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work and technical abilities, too.

## Senior Manager, Technical Writing

The Senior Manager, Technical Writing reports to the UX Director. Technical Writing Managers report to the Senior Manager, Technical Writing.

### Responsibilities

* Hire and manage a world-class team of Technical Writing Managers.
* Actively advocate for Technical Writing throughout the company.
* Hire a world-class team of Technical Writing Managers and Technical Writers to work on their teams
* Help their managers and writers grow their skills and experience.
* Deliver input on promotions, function changes, demotions and offboarding in consultation with the Technical Writing Managers and the UX Director.
* Collaborate with UX Director on quarterly [Technical Writing OKRs](https://about.gitlab.com/company/okrs/) and [Performance Indicators](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/).

#### Documentation

* Ensure teams maintain a single source of truth across our sites, documentation, and tutorials.
* Make docs.gitlab.com an exceptional experience by coordinating UX research, metrics, planning, design, and development work.
* Promote efficiency and usability by ensuring a well-defined content strategy is developed including content reuse capabilities, establishing standards for reuse, and managing implementation.

#### Process Improvement

* Improve the speed, ease, and quality of documentation work by identifying opportunities for process improvement, including automation, and coordinating the implementation and communication of these changes.
* Work with Product Operations to ensure Technical Writing is included appropriately in the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/).

### Requirements

* Experience in setting up online documentation
* Previous experience in using command-line Git.
* Proven experience in managing, mentoring, and training teams of Technical Writers and Technical Writing Managers
* Experience working on production-level documentation
* Self-motivated and strong organizational skills
* Strong written and spoken communication skills
* Familiarity with Static Site Generators, HTML, CSS
* Experience with workflows and tooling used by engineering, operations, product teams
* Familiarity with Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* Able to work with the wider GitLab community.
* Experience collaborating with cross-functional partners at all levels of seniority
* Able to shape documentation process in a high-growth, quickly changing organization
* Able to give clear, timely, and actionable feedback
* You share our [values](https://about.gitlab.com/handbook/values/) and work in accordance with those values.
* [You adhere to our view of Leadership at GitLab.](https://about.gitlab.com/handbook/leadership/#management-group)

### Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

### Hiring Process 

* Screening call with a recruiter.
* Interview with Technical Writer. In this interview, the interviewer will spend time trying to understand the experiences and challenges you've had as a manager, along with the documentation-related tooling you are comfortable with and how you've addressed various content usability considerations.
* Interview with Technical Writing Manager. In this interview, we want to get to know how you think about the place of the tech writing team within an organization, ways of ensuring the team is well-positioned to produce content that has a strong impact on the user experience, and specific examples of large content initiatives. 
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led and your management style. The interviewer will also want to understand how you define strategy, your approach to content usability and improvement, how you've handled difficult situations, and how you've helped mentor technical writers. Do be prepared to talk about your work and technical abilities, too.
* Interview with a VP of Engineering. In this interview, we'll look to understand your leadership experience, management style, and what is driving you to want to join GitLab. 

## Director, Technical Writing

The Director of Technical Writing reports to the UX Director. Senior Technical Writing Managers report to the Director of Technical Writing.

### Responsibilities

* Hire and manage a world-class team of Technical Writing Managers.
* Actively advocate for Technical Writing throughout the company.
* Hire a world-class team of Technical Writing Managers and Technical Writers to work on their teams
* Help their managers and writers grow their skills and experience.
* Deliver input on promotions, function changes, demotions and offboarding in consultation with the Technical Writing Managers and the UX Director.
* Independently manage [Technical Writing OKRs](https://about.gitlab.com/company/okrs/) and [Performance Indicators](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/).
* Communicate significant Technical Writing strategy decisions to senior leadership.
* Manage the Technical Writing budget with oversight from the UX Director.

#### Documentation

* Ensure teams maintain a single source of truth across our sites, documentation, and tutorials.
* Make docs.gitlab.com an exceptional experience by coordinating UX research, metrics, planning, design, and development work.
* Promote efficiency and usability by ensuring a well-defined content strategy is developed including content reuse capabilities, establishing standards for reuse, and managing implementation.

#### Process Improvement

* Improve the speed, ease, and quality of documentation work by identifying opportunities for process improvement, including automation, and coordinating the implementation and communication of these changes.
* Work with Product Operations to ensure Technical Writing is included appropriately in the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/).

### Requirements

* Experience in setting up online documentation
* Previous experience in using command-line Git
* Proven experience in managing, mentoring, and training teams of Technical Writers and Technical Writing Managers
* Experience working on production-level documentation
* Self-motivated and strong organizational skills
* Strong written and spoken communication skills
* Familiarity with Static Site Generators, HTML, CSS
* Experience with workflows and tooling used by engineering, operations, product teams
* Familiarity with Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* Able to work with the wider GitLab community.
* Experience collaborating with cross-functional partners at all levels of seniority
* Able to shape documentation process in a high-growth, quickly changing organization
* Able to give clear, timely, and actionable feedback
* You share our [values](https://about.gitlab.com/handbook/values/) and work in accordance with those values
* [You adhere to our view of Leadership at GitLab.](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

### Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

### Hiring Process 

* Screening call with a recruiter.
* Interview with Technical Writer. In this interview, the interviewer will spend time trying to understand the experiences and challenges you've had as a manager, along with the documentation-related tooling you are comfortable with and how you've addressed various content usability considerations.
* Interview with Technical Writing Manager. In this interview, we want to get to know how you think about the place of the tech writing team within an organization, ways of ensuring the team is well-positioned to produce content that has a strong impact on the user experience, and specific examples of large content initiatives. 
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led and your management style. The interviewer will also want to understand how you define strategy, your approach to content usability and improvement, how you've handled difficult situations, and how you've helped mentor technical writers. Do be prepared to talk about your work and technical abilities, too.
* Interview with a VP of Engineering. In this interview, we'll look to understand your leadership experience, management style, and what is driving you to want to join GitLab
